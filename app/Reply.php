<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = ['message_content', 'subject', 'file_name', 'sender_name', 'sender_address', 'sender_position',
        'receiver_name', 'receiver_address', 'receiver_position', 'sent_date', 'received_date',
        'physical_folder_name', 'user_id'];

    protected $with = ['scannedCopies'];
    public function document(){
        return $this->belongsTo(Document::class);
    }

    public function scannedCopies(){
        return $this->morphMany(ScannedCopy::class, 'scanable');
    }


}
