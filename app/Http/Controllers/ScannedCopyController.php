<?php

namespace App\Http\Controllers;

use App\Document;
use App\Reply;
use App\ScannedCopy;
use App\Traits\APIResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;

class ScannedCopyController extends Controller
{
    use APIResponse;
    public function index(){
        return ScannedCopy::all();
    }

    public function store(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            "file" => "required|max:10000",
            'document_id' => 'required',
            'type' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $file_extension = $request->file->getClientOriginalExtension();
        $file_name = $request->file->getClientOriginalName();
        $date = date('D_H_m_s') . "_" . uniqid(8) . ".{$file_extension}";
        $path = "/public/legal_docs/";
        $mypath = "/storage/legal_docs/". $date;
        $file_path = $request->file('file')->store('public/legal_docs');
//        Storage::put($file_path, $request->file);

        if($request->type == 'document'){
            $document = Document::findOrFail($request->document_id);
            $scannedDocument = $document->scannedCopies()->create(['name' => $file_name, 'path' => $file_path, 'user_id' => $user->id]);

        }
        else {

            $document = Reply::findOrFail($request->document_id);
            $scannedDocument = $document->scannedCopies()->create(['name' => $file_name, 'path' => $file_path, 'user_id' => $user->id]);

        }

        return $scannedDocument;
    }

    public function destroy($file_id){
       $document = ScannedCopy::findOrFail($file_id);
       $document->delete();
       return $document;
    }
}
