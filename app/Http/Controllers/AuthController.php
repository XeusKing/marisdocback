<?php

namespace App\Http\Controllers;

use App\Traits\APIResponse;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Validator;

class AuthController extends Controller
{
    use APIResponse;
    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|min:6',
            'username' => 'required|unique:users|string|min:4|max:255',
            'phone' => 'required|unique:users|string|min:9|max:12',
            'email' => 'required|unique:users|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);

        return $this->login($request);
        return $string;
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user = User::orWhere('username', $request->username)->orWhere('email', $request->username)->orWhere('phone', $request->username)->first();


        if(empty($user)){
            return $this->errorResponse('Username Or Password is Wrong', Response::HTTP_UNAUTHORIZED);
        }


        $user = $user->makeVisible(['password']);
        $password = $user->password;
        Log::debug($user->username);
        Log::debug($user->phone);
        Log::debug($user->password);

        if(Hash::check($request->password, $user->password)){
            $params = [
                'grant_type' => 'password',
                'client_id' => config('services.passport.client_id'),
                'client_secret' => config('services.passport.client_secret'),
                'username' => $request->username,
                'password' => $request->password,
                'scope' => '',
            ];
            $request->request->add($params);
            $proxy = Request::create('oauth/token', 'POST');
            return Route::dispatch($proxy);
            return 'nice ';
        }
        else {
            return $this->errorResponse('Wrong Password or Username or Email or Phone', Response::HTTP_UNAUTHORIZED);
        }
        return $user;
    }


}
