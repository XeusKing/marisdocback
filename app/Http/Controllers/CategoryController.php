<?php

namespace App\Http\Controllers;

use App\Category;
use App\Traits\APIResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class CategoryController extends Controller
{
    use APIResponse;
    public function index(){
        return Category::all();
    }

    public function allCategoriesPaginate($page_number){

        return DB::table('categories')->paginate(20, ['*'], '', $page_number);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:categories',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        return $category = Category::create($request->all());
    }

    public function show($category_id){
        return Category::findOrFail($category_id);
    }

    public function update(Request $request, $category_id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:categories',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $category = Category::findOrFail($category_id);

        $category->fill($request->all());
        if($category->isClean()){
            return $this->errorResponse('Please Change Name', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $category->save();
        return $category;
    }
}
