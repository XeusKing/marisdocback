<?php

namespace App\Http\Controllers;

use App\Document;
use App\Reply;
use App\Traits\APIResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Validator;

class ReplyController extends Controller
{
    use APIResponse;
    public function index(){
        return Reply::all();
    }

    public function store(Request $request){
        $user_id = Auth::id();
        $validator = Validator::make($request->all(), [
            'document_id' => 'required',
            'subject' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $document = Document::findOrFail($request->document_id);

        $request->merge(['user_id' =>$user_id]);

        $reply = $document->replies()->create($request->all());

        return $reply;

    }

    public function show($reply_id){
        return Reply::with('document')->findOrFail($reply_id);
    }

    public function update(Request $request, $reply_id){
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'file_name' => 'required',
            'document_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $reply = Reply::findOrFail($reply_id);

        $reply->fill($request->all());
        if($reply->isClean()){
            return $this->errorResponse('At least one field should be different', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $reply->save();
        return $reply;

    }

}
