<?php

namespace App\Http\Controllers;

use App\Category;
use App\Document;
use App\ScannedCopy;
use App\Traits\APIResponse;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class DocumentController extends Controller
{
    use APIResponse;
    public function index(){
        return Document::all();
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'file_name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $document_file = Document::create($request->all());

        foreach ($request->categories as $category){
            DB::table('category_document')->insert(['category_id' => $category, 'document_id' => $document_file->id]);
        }

        return $document_file;
    }

    public function show($document_id){
//        return $document_id;
        return Document::with('scannedCopies')->findOrFail($document_id);
    }

    public function update(Request $request, $document_id){
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'file_name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $document = Document::findOrFail($document_id);

        $document->fill($request->all());
//        if($document->isClean()){
//            return $this->errorResponse('At least one field should be different', Response::HTTP_UNPROCESSABLE_ENTITY);
//        }

        $categories = DB::table('category_document')->where('document_id', $document_id)->delete();


        foreach ($request->categories as $category){
            DB::table('category_document')->insert(['category_id' => $category, 'document_id' => $document_id]);
        }
        $document->save();
        return $document;

    }

    public function getDocuments($page_number){
//        $documents = DB::table('documents')->paginate(10, ['*'], '', $page_number);
        $documents = Document::with(['categories', 'scannedCopies'])->paginate(10, ['*'], '', $page_number);
        return $documents;
    }

    public function searchDocument(Request $request){
        $validator = Validator::make($request->all(), [
//            'search_content' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $documents = DB::table('documents')
            ->orWhere('subject', 'like', '%'.$request->search_content.'%')
            ->orWhere('file_name', 'like', '%'.$request->search_content.'%')
            ->orWhere('sender_position', 'like', '%'.$request->search_content.'%')
            ->orWhere('receiver_position', 'like', '%'.$request->search_content.'%')
            ->orWhere('receiver_name', 'like', '%'.$request->search_content.'%')
            ->orWhere('sender_name', 'like', '%'.$request->search_content.'%')
            ->orWhere('receiver_address', 'like', '%'.$request->search_content.'%')
            ->orWhere('sender_address', 'like', '%'.$request->search_content.'%')
            ->orWhere('physical_folder_name', 'like', '%'.$request->search_content.'%')
            ->orWhere('sent_date', $request->sent_date)
            ->orWhere('received_date', $request->received_date)
            ->paginate(10, ['*'], '', $request->page_number);

        return $documents;
    }

    public function filterDocument(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $category = Category::with('documents')->where('name', $request->name)->first();

        return $documents = $category->documents()->paginate(10, ['*'], '', $request->page_number);
    }
}
