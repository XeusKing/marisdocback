<?php
namespace App\Traits;

use App\Application;
use App\Endpoint;
use App\Group;
use App\Parameter;
use App\RequestParameter;
use App\ResponseParameter;
use App\SampleCode;
use App\StaticSection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

trait ExtraWork {


    public function differentiate_numbers($number){
        $mtn_array = [650,651,652,653,654,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689];
        $orange_array = [655,656,657,658,659,69];

        if(strlen($number) == 12){

            $last_nine = substr($number, 3, 9);
            $first_three = substr($last_nine, 0, 3);
            $first_two = substr($last_nine, 0, 2);
            if(in_array($first_three, $mtn_array)){
                return (object)['phone' => $number, 'network' => 'mtn', 'currency' => 'EUR'];
            }
            elseif(in_array($first_three, $orange_array) || in_array($first_two, $orange_array)){
                return (object)['phone' => $number, 'network' => 'orange', 'currency' => 'XAF'];
            }
        }
        elseif(strlen($number) == 9){
            $first_three = substr($number, 0, 3);
            $first_two = substr($number, 0, 2);

            if(in_array($first_three, $mtn_array)){
                return (object)['phone' => $number, 'network' => 'mtn','currency' => 'EUR'];
            }
            elseif(in_array($first_three, $orange_array) || in_array($first_two, $orange_array)){
                return (object)['phone' => "237{$number}", 'network' => 'orange','currency' => 'XAF'];
            }
            else
                return 'not ';
        }
        else
            return 'not up to 12';
    }
}
