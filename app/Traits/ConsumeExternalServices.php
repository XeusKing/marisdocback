<?php
namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Http;

trait ConsumeExternalServices {

    /**
     * @param $method
     * @param $requestUrl
     * @param array $formParams
     * @param array $headers
     * @return string
     */
    public function performPost($requestUrl, $formParams = [], $headers = []){
        $response = Http::withHeaders($headers)->post($requestUrl, $formParams);


        $client = $response->clientError();
        $server = $response->serverError();

        if(!empty($client) || !empty($server)){
            $data = json_decode($response);
            if(empty($data)){
                return abort(500, $response);
            }elseif (empty($data->code)){
                return abort(500, $response);
            }
            return abort($data->code, $response);
        }
        return $response;
    }

    public function performGet($requestUrl, $headers = []){
        $response = Http::withHeaders($headers)->get($requestUrl);


        $client = $response->clientError();
        $server = $response->serverError();

        if(!empty($client) || !empty($server)){
            $data = json_decode($response);
            if(empty($data)){
                return abort(500, $response);
            }elseif (empty($data->code)){
                return abort(500, $response);
            }
            return abort($data->code, $response);
        }
        return $response;
    }
}
