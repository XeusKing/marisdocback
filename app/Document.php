<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'subject', 'file_name', 'sender_name', 'sender_address', 'sender_position',
        'receiver_name', 'receiver_address', 'receiver_position', 'sent_date', 'received_date',
        'physical_folder_name',
    ];

    protected $with = ['categories', 'replies', 'scannedCopies'];

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function replies(){
        return $this->hasMany(Reply::class);
    }

    public function scannedCopies(){
        return $this->morphMany(ScannedCopy::class, 'scanable');
    }
}
