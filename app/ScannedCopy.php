<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ScannedCopy extends Model
{
    protected $fillable = ['name', 'path', 'user_id'];


    public function getPathAttribute($path){
        return Storage::url($path);
    }

    public function scanable(){
        return $this->morphTo();
    }
}
