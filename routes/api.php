<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=> 'auth:api', 'prefix' => '/v1/file-system'], function (){
//    Route::post('/auth/login', 'AuthController@login');

    /**
     *
     * Auth User
     */

    Route::get('/auth_user', function (Request $request){
        return $request->user();
    });

    /**
     *
     * Categories
     */

    Route::get('/categories/all_categories/{page_number}', 'CategoryController@allCategoriesPaginate');
    Route::get('/categories', 'CategoryController@index');
    Route::get('/categories/{category_id}', 'CategoryController@show');
    Route::patch('/categories/{category_id}', 'CategoryController@update');
    Route::put('/categories/{category_id}', 'CategoryController@update');

    Route::post('/categories', 'CategoryController@store');

    /**
     *
     * Documents
     */

    Route::get('/documents', 'DocumentController@index');
    Route::post('/documents', 'DocumentController@store');
    Route::patch('/documents/{document_id}', 'DocumentController@update');
    Route::put('/documents/{document_id}', 'DocumentController@update');

    Route::get('/documents/{document_id}', 'DocumentController@show');
    Route::get('/documents/get/{page_number}', 'DocumentController@getDocuments');

    /**
     *
     * Replies
     */
    Route::get('/replies', 'ReplyController@index');
    Route::get('/replies/{reply_id}', 'ReplyController@show');
    Route::patch('/replies/{reply_id}', 'ReplyController@update');
    Route::put('/replies/{reply_id}', 'ReplyController@update');
    Route::post('/replies', 'ReplyController@store');


    /**
     *
     * Files
     */

    Route::get('/files', 'ScannedCopyController@index');
    Route::post('/files', 'ScannedCopyController@store');
    Route::delete('/files/{file_id}', 'ScannedCopyController@destroy');

    /**
     *
     * Search
     */

    Route::post('/search', 'DocumentController@searchDocument');
    Route::post('/filter', 'DocumentController@filterDocument');
});

Route::group(['prefix' => '/v1/file-system'], function (){
    Route::post('/auth/login', 'AuthController@login');
    Route::post('/auth/register', 'AuthController@register');
});
